This module creates a CCK field that accepts RSS urls.

Original version sponsored by Promet Source [http://prometsource.com/]
New version co-sponsored by As If Productions [http://asifproductions.com/]


Features
--------
- Aggregate RSS/Atom feed
- Works with FlexiField
- Controls how many items to display and how frequent are updates
- Feed-enabled nodes may be used in blocks or nodeblocks
- Test page indicates results of a simulated cron job


Types of display
----------------
1. Default
2. Teaser
    - Disregards the type of excerpt indicated in the node. It will always display 200 characters, html stripped
3. Full
    - Disregards the type of excerpt indicated in the node. It will always display the full content.
   

Installation
------------
1. Extract the tarball into your sites/all/modules directory.
2. Enable the module from admin/build/modules


Troubleshooting
---------------

Q: Feeds do not update as quickly as expected.
A: Make sure your cron job is running at least that frequently.

